from rest_framework.generics import ListAPIView

from coins_test.models import Account
from coins_test.serializers.account_serializer import AccountSerializer


class Accounts(ListAPIView):
    """
    List of accounts
    """
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
