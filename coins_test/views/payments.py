from rest_framework.generics import ListAPIView

from coins_test.models.payment import Payment
from coins_test.serializers.payment_serializer import PaymentSerializer


class Payments(ListAPIView):
    """
    List of payments
    """
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
