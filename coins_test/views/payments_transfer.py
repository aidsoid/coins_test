from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from coins_test.models import Account, Payment
from coins_test.serializers.payment_transfer_serializer import PaymentTransferRequestSerializer


class PaymentsTransfer(APIView):
    @swagger_auto_schema(
        request_body=PaymentTransferRequestSerializer,
        responses={
            '400': 'Bad request',
            '403': 'Forbidden for some reason',
            '500': 'Internal server error',
            '201': 'Transfer completed',
        },
    )
    def post(self, request, *args, **kwargs):
        """
        Transfer funds from one account to another
        :param request: Request
        :return: Response
        """
        request_serializer = PaymentTransferRequestSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)

        from_account_id = request_serializer.validated_data.get('from_account_id')
        to_account_id = request_serializer.validated_data.get('to_account_id')
        amount = request_serializer.validated_data.get('amount')

        # check: is sender's account exists
        try:
            # lock selected account for update
            from_account = Account.objects.select_for_update().get(id=from_account_id)
        except Account.DoesNotExist:
            msg = f'Account with id {from_account_id} does not exist'
            return Response(data={'msg': msg}, status=status.HTTP_400_BAD_REQUEST)

        # check: is recipient's account exists
        try:
            # lock selected account for update
            to_account = Account.objects.select_for_update().get(id=to_account_id)
        except Account.DoesNotExist:
            msg = f'Account with id {to_account_id} does not exist'
            return Response(data={'msg': msg}, status=status.HTTP_400_BAD_REQUEST)

        # check: only positive amount
        if amount <= 0:
            msg = f'Incorrect amount: {amount}'
            return Response(data={'msg': msg}, status=status.HTTP_403_FORBIDDEN)

        # start transaction
        with transaction.atomic():
            # when the queryset is evaluated, all matched entries will be locked until the end of the transaction block

            # check: from_account & to_account have same currencies
            if from_account.currency != to_account.currency:
                msg = "Sender's & recipient's account have different currencies"
                return Response(data={'msg': msg}, status=status.HTTP_403_FORBIDDEN)

            # check: balance can't go below zero
            if from_account.balance - amount < 0:
                msg = f'Insufficient funds: {amount}'
                return Response(data={'msg': msg}, status=status.HTTP_403_FORBIDDEN)

            # update from_account balance
            from_account.balance -= amount
            from_account.save()
            # update to_account balance
            to_account.balance += amount
            to_account.save()

            payments_for_create = (
                # create outgoing payment
                Payment(
                    account=from_account,
                    amount=amount,
                    direction=Payment.OUTGOING,
                    to_account=to_account,
                ),
                # create incoming payment
                Payment(
                    account=to_account,
                    amount=amount,
                    direction=Payment.INCOMING,
                    from_account=from_account
                )
            )
            Payment.objects.bulk_create(payments_for_create)

        msg = 'Transfer completed'
        return Response(data={'msg': msg}, status=status.HTTP_201_CREATED)
