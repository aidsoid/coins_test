from coins_test.views.accounts import Accounts
from coins_test.views.payments import Payments
from coins_test.views.payments_transfer import PaymentsTransfer

__all__ = [
    Accounts,
    Payments,
    PaymentsTransfer,
]
