from rest_framework import serializers

from coins_test.models import Account


class AccountSerializer(serializers.ModelSerializer):
    """
    Serializer for Account
    """
    class Meta:
        model = Account
        fields = ['id', 'balance', 'currency']
