from rest_framework import serializers

from coins_test.models.payment import Payment


class PaymentSerializer(serializers.ModelSerializer):
    """
    Serializer for Payment
    """
    class Meta:
        model = Payment
        fields = ['id', 'account', 'amount', 'direction', 'to_account', 'from_account']
