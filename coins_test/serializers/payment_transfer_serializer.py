from rest_framework import serializers

from coins_test.models import Account
from coins_test.models.payment import Payment


class PaymentTransferRequestSerializer(serializers.Serializer):
    """
    Input serializer for payment transfer operation
    """
    from_account_id = serializers.ModelField(model_field=Account()._meta.get_field('id'), required=True)
    to_account_id = serializers.ModelField(model_field=Account()._meta.get_field('id'), required=True)
    amount = serializers.ModelField(model_field=Payment()._meta.get_field('amount'), required=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    class Meta:
        fields = ['from_account', 'to_account', 'amount']
