from django.apps import AppConfig


class CoinsTestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'coins_test'
