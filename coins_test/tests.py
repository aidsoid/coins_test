from decimal import Decimal

from django.test import Client
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from coins_test.models import Account, Payment


class ApiTestCase(TestCase):
    """
    Tests for API methods
    """
    def setUp(self):
        """
        Common code that calls before any test
        """
        # test client for making requests
        self.c = Client()

        # create accounts in db
        self.accounts = [
            Account(
                id='bob123',
                balance='100.00',
                currency=Account.USD
            ),
            Account(
                id='alice456',
                balance='0.01',
                currency=Account.USD
            )
        ]
        Account.objects.bulk_create(self.accounts)

    def test_accounts_endpoint(self):
        """
        Test for accounts api endpoint
        """
        # get accounts list via api
        response = self.c.get(reverse('accounts'))

        # check: is correct status
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # check: is same count of instances
        received_accounts = response.data
        self.assertEqual(len(received_accounts), len(self.accounts))
        # check: is same objects fields values
        for i, account in enumerate(self.accounts):
            self.assertEqual(received_accounts[i]['id'], self.accounts[i].id)
            self.assertEqual(received_accounts[i]['balance'], self.accounts[i].balance)
            self.assertEqual(received_accounts[i]['currency'], self.accounts[i].currency)

    def test_payments_endpoint(self):
        """
        Test for payments api endpoint
        """
        # simulate money transfer from one account to another
        from_account = self.accounts[0]
        to_account = self.accounts[1]
        payments = [
            Payment(
                account=from_account,
                amount=Decimal('100.00'),
                direction=Payment.OUTGOING,
                to_account=to_account
            ),
            Payment(
                account=to_account,
                amount=Decimal('100.00'),
                direction=Payment.OUTGOING,
                from_account=from_account
            )
        ]
        Payment.objects.bulk_create(payments)

        # get accounts list via api
        response = self.c.get(reverse('payments'))

        # check: is correct status
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # check: is same count of instances
        received_payments = response.data
        self.assertEqual(len(received_payments), len(payments))
        # check: is same objects fields values
        for i, account in enumerate(self.accounts):
            account_id = payments[i].account.id if payments[i].account else None
            self.assertEqual(received_payments[i]['account'], account_id)
            self.assertEqual(Decimal(received_payments[i]['amount']), payments[i].amount)
            self.assertEqual(received_payments[i]['direction'], payments[i].direction)
            from_account_id = payments[i].from_account.id if payments[i].from_account else None
            self.assertEqual(received_payments[i]['from_account'], from_account_id)
            to_account_id = payments[i].to_account.id if payments[i].to_account else None
            self.assertEqual(received_payments[i]['to_account'], to_account_id)

    def test_payments_transfer_endpoint_sufficient_funds(self):
        """
        Test for payments_transfer api endpoint
        Case: The sender's account has sufficient funds
        """
        # make a transfer: the sender's account has sufficient funds
        from_account = self.accounts[0]
        to_account = self.accounts[1]
        amount_for_transfer = Decimal('100.00')
        payloads = {
            'from_account_id': from_account.id,
            'to_account_id': to_account.id,
            'amount': amount_for_transfer,
        }
        response = self.c.post(reverse('payments_transfer'), payloads, content_type='application/json')

        # check: is correct status
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # check: is correct balance on sender's account
        desired_from_account_balance = Decimal(from_account.balance) - amount_for_transfer
        from_account.refresh_from_db()
        self.assertEqual(from_account.balance, desired_from_account_balance)
        # check: is correct balance on recipient's account
        desired_to_account_balance = Decimal(to_account.balance) + amount_for_transfer
        to_account.refresh_from_db()
        self.assertEqual(to_account.balance, desired_to_account_balance)
        # check: is correct payments count
        try:
            outgoing_payment = Payment.objects.get(account=from_account)
        except Payment.DoesNotExist:
            outgoing_payment = None
        # desired that 1 outgoing payment will be created for from_account
        self.assertNotEqual(outgoing_payment, None)
        try:
            incoming_payment = Payment.objects.get(account=to_account)
        except Payment.DoesNotExist:
            incoming_payment = None
        # desired that 1 incoming payment will be created for to_account
        self.assertNotEqual(incoming_payment, None)
        # check: is correct outgoing payment fields
        self.assertEqual(outgoing_payment.account, from_account)
        self.assertEqual(outgoing_payment.amount, amount_for_transfer)
        self.assertEqual(outgoing_payment.direction, Payment.OUTGOING)
        self.assertEqual(outgoing_payment.from_account, None)
        self.assertEqual(outgoing_payment.to_account, to_account)
        # check: is correct incoming payment fields
        self.assertEqual(incoming_payment.account, to_account)
        self.assertEqual(incoming_payment.amount, amount_for_transfer)
        self.assertEqual(incoming_payment.direction, Payment.INCOMING)
        self.assertEqual(incoming_payment.from_account, from_account)
        self.assertEqual(incoming_payment.to_account, None)

    def test_payments_transfer_endpoint_insufficient_funds(self):
        """
        Test for payments_transfer api endpoint
        Case: The sender's account has insufficient funds
        """
        # make a transfer: the sender's account has insufficient funds
        from_account = self.accounts[0]
        to_account = self.accounts[1]
        amount_for_transfer = Decimal('5000.00')
        payloads = {
            'from_account_id': from_account.id,
            'to_account_id': to_account.id,
            'amount': amount_for_transfer,
        }
        response = self.c.post(reverse('payments_transfer'), payloads, content_type='application/json')

        # check: is correct status
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # check: is correct balance on sender's account
        desired_from_account_balance = Decimal(from_account.balance)
        from_account.refresh_from_db()
        self.assertEqual(from_account.balance, desired_from_account_balance)
        # check: is correct balance on recipient's account
        desired_to_account_balance = Decimal(to_account.balance)
        to_account.refresh_from_db()
        self.assertEqual(to_account.balance, desired_to_account_balance)
        # check: is correct payments count
        try:
            outgoing_payment = Payment.objects.get(account=from_account)
        except Payment.DoesNotExist:
            outgoing_payment = None
        # desired that outgoing payment will not be created
        self.assertEqual(outgoing_payment, None)
        try:
            incoming_payment = Payment.objects.get(account=to_account)
        except Payment.DoesNotExist:
            incoming_payment = None
        # desired that incoming payment will not be created
        self.assertEqual(incoming_payment, None)

    def test_payments_transfer_endpoint_currency_differs(self):
        """
        Test for payments_transfer api endpoint
        Case: The sender's account currency differs than the recipient's account currency
        """
        # make a transfer: the sender's account currency differs than the recipient's account currency
        # make a transfer: the sender's account has insufficient funds
        from_account = self.accounts[0]
        # set currency that differs from to_account currency
        from_account.currency = Account.EUR
        from_account.save()
        to_account = self.accounts[1]
        amount_for_transfer = Decimal('100.00')
        payloads = {
            'from_account_id': from_account.id,
            'to_account_id': to_account.id,
            'amount': amount_for_transfer,
        }
        response = self.c.post(reverse('payments_transfer'), payloads, content_type='application/json')

        # check: is correct status
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # check: is correct balance on sender's account
        desired_from_account_balance = Decimal(from_account.balance)
        from_account.refresh_from_db()
        self.assertEqual(from_account.balance, desired_from_account_balance)
        # check: is correct balance on recipient's account
        desired_to_account_balance = Decimal(to_account.balance)
        to_account.refresh_from_db()
        self.assertEqual(to_account.balance, desired_to_account_balance)
        # check: is correct payments count
        try:
            outgoing_payment = Payment.objects.get(account=from_account)
        except Payment.DoesNotExist:
            outgoing_payment = None
        # desired that outgoing payment will not be created
        self.assertEqual(outgoing_payment, None)
        try:
            incoming_payment = Payment.objects.get(account=to_account)
        except Payment.DoesNotExist:
            incoming_payment = None
        # desired that incoming payment will not be created
        self.assertEqual(incoming_payment, None)
