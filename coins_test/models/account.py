from django.db import models
from django.utils.translation import ugettext_lazy as _

from coins_test.models.timestamped_abstract import TimestampedAbstractModel


class Account(TimestampedAbstractModel):
    """
    Account
    """
    # currencies in ISO 4217 format
    # better to create separate model for currencies, but not for test task
    USD = 'USD'
    EUR = 'EUR'
    CURRENCY_CHOICES = (
        (USD, _('US Dollar')),
        (EUR, _('Euro'))
    )

    id = models.CharField(
        max_length=255,
        verbose_name=_('Account identifier'),
        help_text=_('Account identifier'),
        primary_key=True
    )
    balance = models.DecimalField(
        max_digits=19, decimal_places=2,
        verbose_name=_('Balance'),
        help_text=_('Current account balance')
    )
    currency = models.CharField(
        max_length=3,
        choices=CURRENCY_CHOICES,
        verbose_name=_('Currency'),
        help_text=_('Currency code in ISO 4217 format')
    )

    class Meta:
        verbose_name = _('Account')
        verbose_name_plural = _('Accounts')

    def __str__(self):
        return f'[{self.pk}] Account with balance {self.balance} {self.currency}'
