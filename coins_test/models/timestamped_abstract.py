from django.db import models
from django.utils.translation import ugettext_lazy as _


class TimestampedAbstractModel(models.Model):
    """
    Abstract model with common datetime fields
    """
    created = models.DateTimeField(_('Created time'), auto_now_add=True)
    modified = models.DateTimeField(_('Modified time'), auto_now=True)

    class Meta:
        abstract = True
        ordering = ("-created",)
