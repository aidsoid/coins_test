from django.db import models
from django.utils.translation import ugettext_lazy as _

from coins_test.models.timestamped_abstract import TimestampedAbstractModel


class Payment(TimestampedAbstractModel):
    """
    Payment
    """
    # payment directions
    OUTGOING = 'outgoing'
    INCOMING = 'incoming'
    PAYMENT_DIRECTION_CHOICES = (
        (OUTGOING, _('Outgoing')),
        (INCOMING, _('Incoming')),
    )

    account = models.ForeignKey(
        'Account',
        on_delete=models.PROTECT,
        verbose_name=_('Account'),
        help_text=_('Account'),
        related_name='payments'
    )
    amount = models.DecimalField(
        max_digits=19, decimal_places=2,
        verbose_name=_('Amount'),
        help_text=_('Payment amount')
    )
    direction = models.CharField(
        max_length=10,
        choices=PAYMENT_DIRECTION_CHOICES,
        verbose_name=_('Direction'),
        help_text=_('Payment direction')
    )
    from_account = models.ForeignKey(
        'Account',
        blank=True, null=True,
        on_delete=models.PROTECT,
        verbose_name=_('From account'),
        help_text=_('From account'),
        related_name='payments_from_account'
    )
    to_account = models.ForeignKey(
        'Account',
        blank=True, null=True,
        on_delete=models.PROTECT,
        verbose_name=_('To account'),
        help_text=_('To account'),
        related_name='payments_to_account'
    )

    class Meta:
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')

    def __str__(self):
        return f'[{self.pk}] {self.direction} payment'
