from django.conf.urls import url
from django.urls import path, include

from coins_test import views

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Wallet API",
      default_version='v1',
      description="It's just a test task for coins.pl.",
      contact=openapi.Contact(name="Alexey Doroshenko", email="aidsoid@gmail.com"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    # swagger description in yaml format
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    # swagger description
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    # redoc
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    # API v1
    path('api/v1/', include([
        # GET - accounts list
        path('accounts/', views.Accounts.as_view(), name='accounts'),
        # GET - payments list
        path('payments/', views.Payments.as_view(), name='payments'),
        # POST - transfer funds from one account to another
        path('payments/transfer/', views.PaymentsTransfer.as_view(), name='payments_transfer'),
    ])),
]
