from django.contrib import admin

from coins_test.models import Account, Payment


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    pass
