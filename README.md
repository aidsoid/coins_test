# Wallet service with a RESTful API

## Project's purpose
It's an implementation of generic Wallet service with a RESTful API.

### Initial user stories

* I want to be able to send a payment from one account to another (same currency)
* I want to be able to see all payments
* I want to be able to see available accounts

### Initial requirements
* Only payments within the same currency are supported (no exchanges)
* There are no users in the system (no auth)
* Balance can't go below zero
* More than one instance of the application can be launched

It's just a test task for coins.pl.

[More info](./docs/go_or_python_engineer.pdf)

## How to set it up

### Build docker image
```bash
make docker-build
```
### Run docker image
```bash
make docker-run
```

After service is started you can send requests to API, for example (default port is 8000):

```
Accounts list:
GET http://127.0.0.1:8000/api/v1/account/
Payments list: 
GET http://127.0.0.1:8000/api/v1/payments/
Transfer funds from one account to another:
POST http://127.0.0.1:8000/api/v1/payments/transfer/
Swagger documentation:
GET http://127.0.0.1:8000/swagger/
```

## Start contributing

Prepare project and run:
```bash
# create virtual environment
python3 -m venv env
# activate virtual environment
source env/bin/activate
# install requirements for project
pip install -r requirements.txt
# run development server
python manage.py runserver
```

For most frequently used commands you can use make utility, see [Makefile](Makefile) at root dir.

## Code linting

As code linter project uses [flake8](https://flake8.pycqa.org/en/latest/).
Settings for flake8 stored in [.flake8](.flake8) file.

To run code linter:
```bash
make local-flake
```

## Run tests
Project use a Python standard library module:
[unittest](https://docs.python.org/3/library/unittest.html#module-unittest).

To run unit tests:
```bash
make local-test
```
