# You can run targets: make %target-name%
# For example, to run tests: make local-test

# virtual environment dir
ENV=env/bin/python

# =================================================
# LOCAL
# =================================================
local-runserver:
	# run development server
	$(ENV) manage.py runserver

local-makemigrations:
	# make migrations
	$(ENV) manage.py makemigrations

local-migrate:
	# migrate
	$(ENV) manage.py migrate

local-showmigrations:
	# show migrations
	$(ENV) manage.py showmigrations basics

local-flake:
	# run linter
	$(ENV) -m flake8

local-test:
	# run tests
	$(ENV) manage.py test

# =================================================
# DOCKER
# =================================================
docker-build:
	# build docker image
	docker build -t coins_test:latest .

docker-run:
	# run app in doсker
	docker run -p 8000:8000 --env APP_DATABASE_HOST=host.docker.internal coins_test:latest
