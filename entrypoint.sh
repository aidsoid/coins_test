#!/bin/bash

set -ex

[ -z "$GUNICORN_BIND" ]           && GUNICORN_BIND=:8000
[ -z "$GUNICORN_WORKERS" ]        && GUNICORN_WORKERS=2
[ -z "$GUNICORN_WORKER_CLASS" ]   && GUNICORN_WORKER_CLASS=gthread
[ -z "$GUNICORN_WORKER_TMP_DIR" ] && GUNICORN_WORKER_TMP_DIR=/dev/shm
[ -z "$GUNICORN_LOG_FILE" ]       && GUNICORN_LOG_FILE=-
[ -z "$GUNICORN_LOG_LEVEL" ]      && GUNICORN_LOG_LEVEL=info

printenv
python manage.py migrate

exec gunicorn project.wsgi \
    --bind $GUNICORN_BIND \
    --workers $GUNICORN_WORKERS \
    --worker-class $GUNICORN_WORKER_CLASS \
    --worker-tmp-dir $GUNICORN_WORKER_TMP_DIR \
    --log-file $GUNICORN_LOG_FILE \
    --log-level $GUNICORN_LOG_LEVEL \
"$@"
