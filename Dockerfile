FROM python:3.9.5-slim AS coins-python-env

ENV PYTHONUNBUFFERED=1

WORKDIR /code

RUN set -x \
    && pip install pip --upgrade \
    && pip install gunicorn==20.0.4 \
    && apt update \
    && apt clean

COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/

EXPOSE 8000

ENTRYPOINT ["/bin/bash", "/code/entrypoint.sh"]
